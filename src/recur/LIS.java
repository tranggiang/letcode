package recur;

import java.util.Arrays;

import util.MyUtil;

public class LIS {
	static int totalMax = 1;

	public static void main(String[] args) {
		int[] randomArray = MyUtil.randomArray(5);
		System.out.println(Arrays.toString(randomArray));

		for (int i = 0; i < randomArray.length; i++)
			LIS(randomArray, i);

		System.out.println(totalMax);

	}

	private static int LIS(int[] arr, int index) {
		if (index == 0)
			return 1;
		int max = 0;
		for (int i = 0; i < index; i++) {
			if (arr[i] < arr[index]) {
				int localMax = LIS(arr, i);
				if (localMax > max)
					max = localMax;
			}

		}
		if (max + 1 > totalMax)
			totalMax = max + 1;
		return max + 1;

	}

}
