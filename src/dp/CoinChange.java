package dp;

import java.util.Arrays;

public class CoinChange {

	public static void main(String[] args) {
		Solutio20 solutio20 = new Solutio20();
		int amount = 11;
		int[] coins = { 2, 2, 5 };
		int minCoinChange = solutio20.coinChangeInter(coins, amount);
		System.out.println(minCoinChange);
	}

}

class Solutio20 {
	public int coinChange(int[] coins, int amount) {
		if (amount == 0)
			return 0;
		if (amount < 0)
			return -1;
		int ans = Integer.MAX_VALUE;
		for (int coin : coins) {

			int ret = coinChange(coins, amount - coin);
			if (ret >= 0 && ret + 1 < ans)
				ans = ret + 1;

		}

		return ans == Integer.MAX_VALUE ? -1 : ans;
	}

	public int coinChangeInter(int[] coins, int amount) {

		int[] dp = new int[amount + 1];
		Arrays.fill(dp, amount + 1);
		dp[0] = 0;
		for (int i = 0; i < dp.length; i++) {
			for (int coin : coins) {
				// array out of index
				if (i + coin > amount || i + coin < 0)
					continue;
				dp[i + coin] = Math.min(dp[i + coin], dp[i] + 1);
			}
		}
		return dp[amount] == 0 ? -1 : dp[amount];
	}
}
