package letcode;

public class MoveZeroes {

	public static void main(String[] args) {
		int[] nums = { 0, 1, 0, 3, 12 };
		moveZeroes(nums);
		System.out.println(nums);
	}

	public static void moveZeroes(int[] nums) {
		int lastNonZero = 0;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != 0) {
				nums[lastNonZero++] = nums[i];
			}
		}
		for (int i = lastNonZero; i < nums.length; i++) {
			nums[i] = 0;
		}
	}

}
