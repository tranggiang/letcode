package backtrack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

//https://leetcode.com/problems/largest-divisible-subset/
public class LargestDivisibleSubset {

	public static void main(String[] args) {
		Solution368 solution368 = new Solution368();
		int[] nums = { 1, 2, 4, 6, 4, 8 };
		List<Integer> ans = solution368.largestDivisibleSubset(nums);
		System.out.println(ans.toString());
	}

}

class Solution368 {
	private List<Integer> ans = new ArrayList();
	private Map<Integer, List<Integer>> memo = new HashMap();

	public List<Integer> largestDivisibleSubset(int[] nums) {
		Arrays.sort(nums);

		/*
		 * for (int i = 0; i < nums.length; i++) { List<Integer> maxSubset =
		 * goodHelper(nums, i); if (maxSubset.size() > ans.size()) ans = maxSubset; }
		 */

		// badHelper(nums, 0, new LinkedList<>());
		forloop(nums);
		return ans;

	}

	private void forloop(int[] nums) {

		for (int i = 0; i < nums.length; i++) {
			List<Integer> maxList = new ArrayList<>();
			for (int j = 0; j < i; j++) {
				if (nums[i] % nums[j] == 0) {
					List<Integer> sublist = memo.get(j);
					if (sublist.size() > maxList.size())
						maxList = sublist;
				}
			}
			List<Integer> ret = new ArrayList<>(maxList);
			ret.add(nums[i]);
			if (ret.size() > ans.size())
				ans = ret;
			memo.put(i, ret);
		}

	}

	private List<Integer> goodHelper(int[] nums, int start) {
		if (memo.containsKey(start))
			return memo.get(start);
		List<Integer> maxList = new ArrayList<>();
		for (int i = 0; i < start; i++) {
			if (nums[start] % nums[i] == 0) {
				List<Integer> sublist = goodHelper(nums, i);
				if (sublist.size() > maxList.size())
					maxList = sublist;
			}
		}

		List<Integer> ret = new ArrayList<>(maxList);
		ret.add(nums[start]);
		memo.put(start, ret);
		return ret;
	}

	private void badHelper(int[] nums, int start, LinkedList<Integer> comb) {
		System.out.println("Start : " + start);
		System.out.println("Comb : " + comb.toString());
		if (comb.size() > ans.size()) {
			ans = new ArrayList<>(comb);
		}

		for (int i = start; i < nums.length; i++) {
			if (comb.isEmpty() || nums[i] % comb.getLast() == 0) {
				comb.add(nums[i]);
				badHelper(nums, i + 1, comb);
				comb.removeLast();
			}
		}
	}

}
