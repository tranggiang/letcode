package backtrack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PermutationsII {

	public static void main(String[] args) {
		Solution solution = new Solution();
		int[] nums = new int[] { 1, 1, 2 };
		List<List<Integer>> ans = solution.permuteUnique(nums);
		System.out.println(ans.toString());
	}

}

class Solution {
	List<List<Integer>> ans = new ArrayList();
	int l;

	public List<List<Integer>> permuteUnique(int[] nums) {

		Map<Integer, Integer> map = new HashMap();
		l = nums.length;
		for (int num : nums)
			map.put(num, map.getOrDefault(num, 0) + 1);

		backtrack(map, new ArrayList());
		return ans;
	}

	private void backtrack(Map<Integer, Integer> map, List<Integer> comb) {

		if (comb.size() == l) {
			ans.add(new ArrayList(comb));
			return;
		}

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {

			Integer num = entry.getKey();
			Integer counter = entry.getValue();

			if (counter == 0) {
				continue;
			}
			comb.add(num);
			map.put(num, counter - 1);

			backtrack(map, comb);

			comb.remove(comb.size() - 1);
			map.put(num, counter);

		}
	}
}
