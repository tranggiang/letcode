package letcode;

//https://leetcode.com/problems/rotate-array/
//https://www.youtube.com/watch?v=gmu0RA5_zxs
public class RotateArray {

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3, 4, 5, 6, 7 };
		int k = 3;
		rotate(nums, k);
		System.out.println(nums);
	}

	public static void rotate(int[] nums, int k) {
		int l = nums.length;
		int[] ans = new int[l];
		for (int i = 0; i < l; i++) {
			int mod = (i + k) % l;
			ans[mod] = nums[i];
		}

	}
}
