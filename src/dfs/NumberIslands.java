package dfs;

public class NumberIslands {

	public static void main(String[] args) {
		Solution solution = new Solution();
		char[][] grid = new char[][] { { '1', '1', '1' }, { '0', '1', '0' }, { '1', '1', '1' } };
		int numIslands = solution.numIslands(grid);
		System.out.println(numIslands);
	}

}

class Solution {
	private int[][] visited;

	public int numIslands(char[][] grid) {
		visited = new int[grid.length][grid[0].length];
		int count = 0;
		for (int r = 0; r < grid.length; r++)
			for (int c = 0; c < grid[0].length; c++)
				if (visited[r][c] == 0 && grid[r][c] == '1') {
					visited[r][c] = 1;
					count++;
					explore(grid, r, c);
				}

		return count;

	}

	private void explore(char[][] grid, int r, int c) {
		if (r + 1 < grid.length && grid[r + 1][c] == '1') {
			visited[r + 1][c] = 1;
			explore(grid, r + 1, c);
		}
		if (c + 1 < grid[0].length && grid[r][c + 1] == '1') {
			visited[r][c + 1] = 1;
			explore(grid, r, c + 1);
		}
	}
}
