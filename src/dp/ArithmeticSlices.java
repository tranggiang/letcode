package dp;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.com/problems/arithmetic-slices/
public class ArithmeticSlices {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

class Solution12 {
	private Map<String, Boolean> memo = new HashMap();
	private int sum = 0;

	public int numberOfArithmeticSlices(int[] nums) {

		int[][] dp = new int[nums.length][nums.length];
		int count = 0;
		for (int i = 0; i + 2 < nums.length; i++) {
			if (nums[i] + nums[i + 2] == 2 * nums[i + 1]) {
				dp[i][i + 2] = 1;
				count++;
			}
		}
		for (int i = 0; i < nums.length; i++) {
			for (int j = i + 3; j < nums.length; j++) {
				if (dp[i][j - 1] == 1 && nums[j] + nums[j - 2] == 2 * nums[j - 1]) {
					dp[i][j] = 1;
					count++;
				}
			}
		}

		return count;

	}

	public int numberOfArithmeticSlicesRecur(int[] nums) {
		if (nums.length < 3)
			return 0;

		for (int i = 0; i + 2 < nums.length; i++) {
			numberOfArithmeticSlicesHelper(nums, i, i + 2);
		}
		return sum;
	}

	private void numberOfArithmeticSlicesHelper(int[] nums, int start, int end) {
		if (end >= nums.length)
			return;
		if (end - start == 2) {
			if (2 * nums[start + 1] == nums[end] + nums[start]) {
				sum++;
				memo.put(start + "" + end, true);
				numberOfArithmeticSlicesHelper(nums, start, end + 1);
			}

		}

		else {
			if (2 * nums[end - 1] == nums[end] + nums[end - 2] && memo.get(start + "" + (end - 1))) {
				sum++;
				memo.put(start + "" + end, true);
				numberOfArithmeticSlicesHelper(nums, start, end + 1);
			}
		}

	}
}
