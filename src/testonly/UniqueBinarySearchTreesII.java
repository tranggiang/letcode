package testonly;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.TreeNode;

public class UniqueBinarySearchTreesII {

	public static void main(String[] args) {
		int n = 3;
		List<Integer> numbers = new ArrayList<>();
		for (int i = 1; i <= n; i++) {
			numbers.add(i);
		}
		generateTrees(numbers);
	}

	public static List<TreeNode> generateTrees(List<Integer> numbers) {
		int size = numbers.size();
		TreeNode root;
		for (int i = 1; i <= size; i++) {
			Map<Integer, Integer> procesed = new HashMap();
			procesed.put(i, i);
			root = new TreeNode(i, null, null);

			findChild(root, numbers, procesed);

			printTree(root);
		}

		return null;

	}

	private static void printTree(TreeNode root) {
		System.out.println(root.val);
		while (root.left != null) {
			root = root.left;
			System.out.println(root.val);
		}
		while (root.right != null) {
			root = root.right;
			System.out.println(root.val);
		}
	}

	private static void findChild(TreeNode root, List<Integer> numbers, Map<Integer, Integer> procesed) {
		TreeNode left = null;
		TreeNode right = null;
		if (procesed.size() == numbers.size())
			return;
		for (int i : numbers) {
			if (procesed.containsKey(i))
				continue;
			if (i <= root.val) {
				left = new TreeNode(i, null, null);
				root.left = left;
				procesed.put(i, i);
			}

		}
		if (left != null)
			findChild(left, numbers, procesed);
		for (int i : numbers) {
			if (procesed.containsKey(i))
				continue;
			if (i > root.val) {
				right = new TreeNode(i, null, null);
				root.right = right;
				procesed.put(i, i);
			}

		}
		if (left != null)
			findChild(right, numbers, procesed);
	}
}
