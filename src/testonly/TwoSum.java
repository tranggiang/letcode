package testonly;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import util.MyUtil;

//c5.23
public class TwoSum {

	public static void main(String[] args) {
		int[] array = MyUtil.randomArray(10);
		System.out.println("input : " + Arrays.toString(array));
		checkSum(array);
	}

	private static void checkSum(int[] array) {
		if (array.length < 3) {
			throw new IllegalArgumentException();
		}
		Map<Integer, Integer> map = new HashMap(array.length);
		map.put(array[0], 0);
		map.put(array[1], 1);

		for (int i = 2; i < array.length; i++) {
			for (int j = 0; j < i; j++) {
				int diff = array[i] - array[j];
				Integer index = map.get(diff);
				if (index != null && index != j) {
					System.out.println("indexs  i : " + i + " j: " + j + " k : " + index);
				}
				map.put(array[i], i);
			}
		}
	}

}
