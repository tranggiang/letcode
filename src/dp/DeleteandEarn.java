package dp;

import java.util.Map;
import java.util.Map.Entry;

public class DeleteandEarn {

	public static void main(String[] args) {
		Solution740 solution740 = new Solution740();
		int[] nums = { 3, 4, 2 };
		int deleteAndEarn = solution740.deleteAndEarn(nums);
		System.out.println(deleteAndEarn);

	}

}

class Solution740 {

	private int max = 0;

	public int deleteAndEarn(int[] nums) {

		// Map<Integer, Integer> map = new HashMap();
		// for (int num : nums)
		// map.put(num, map.getOrDefault(num, 0) + 1);
		// bas(map, 0);
		int[] dp = new int[10001];

		for (int num : nums)
			dp[num] += num;

		for (int i = 2; i < 10001; i++) {
			dp[i] = Math.max(dp[i] + dp[i - 2], dp[i - 1]);
		}

		return dp[10000];

	}

	private void bad(Map<Integer, Integer> map, int findMax) {
		for (Entry<Integer, Integer> entry : map.entrySet()) {
			int key = entry.getKey();
			int value = entry.getValue();
			if (value == 0)
				continue;
			findMax += key * value;
			max = Math.max(findMax, max);
			entry.setValue(0);

			Integer valuePlusOne = map.get(key + 1);
			Integer valueMinusOne = map.get(key - 1);
			map.computeIfPresent(key + 1, (k, v) -> 0);
			map.computeIfPresent(key - 1, (k, v) -> 0);
			bad(map, findMax);
			findMax -= key * value;
			entry.setValue(value);

			if (valuePlusOne != null) {
				map.put(key + 1, valuePlusOne);
			}
			if (valueMinusOne != null) {
				map.put(key - 1, valueMinusOne);
			}

		}

	}
}
