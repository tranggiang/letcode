package dp;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.com/problems/delete-operation-for-two-strings/
//https://beizhedenglong.github.io/leetcode-solutions/docs/delete-operation-for-two-strings
public class DeleteOperationTwoStrings {

	public static void main(String[] args) {
		Solution17 solution17 = new Solution17();
		String word1 = "sea";
		String word2 = "eat";
		int minDistance = solution17.minDistanceInter(word1, word2);
		System.out.println(minDistance);
	}

}

class Solution17 {
	private Map<String, Integer> memo = new HashMap<>();
	private String word1;
	private String word2;
	private int[][] dp;

	public int minDistance(String word1, String word2) {
		this.word1 = word1;
		this.word2 = word2;
		return minDistanceHelper();

	}

	public int minDistanceInter(String word1, String word2) {
		dp = new int[word1.length() + 1][word2.length() + 1];

		for (int i = 0; i < word1.length(); i++) {
			dp[i][word2.length()] = word1.length() - i;
		}
		for (int i = 0; i < word2.length(); i++) {
			dp[word1.length()][i] = word2.length() - i;
		}

		for (int i = word1.length() - 1; i >= 0; i--)
			for (int j = word2.length() - 1; j >= 0; j--) {

				if (word1.charAt(i) == word2.charAt(j)) {
					dp[i][j] = dp[i + 1][j + 1];
				} else {
					// TODO think about what dp[i+1][j] or dp[i][j+1] value should be
					dp[i][j] = 1 + Math.min(dp[i + 1][j], dp[i][j + 1]);
				}

			}

		return dp[0][0];
	}

	private int minDistanceHelper() {
		if (word1.length() == 0 && word2.length() == 0)
			return 0;
		if (word1.length() == 0)
			return word2.length();
		if (word2.length() == 0)
			return word1.length();
		if (word1.charAt(0) == word2.charAt(0)) {
			return minDistance(word1.substring(1), word2.substring(1));
		} else {
			return 1 + Math.min(minDistance(word1.substring(1), word2), minDistance(word1, word2.substring(1)));
		}
	}
}
