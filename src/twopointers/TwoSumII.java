package twopointers;

import java.util.Arrays;

public class TwoSumII {

	public static void main(String[] args) {
		int[] myIntArray = { 1, 2, 3, 6, 7, };
		int[] ret = twoSum(myIntArray, 5);
		System.out.println(Arrays.toString(ret));
	}

	public static int[] twoSum(int[] numbers, int target) {

		int[] ans = new int[2];
		int start = 0;
		int end = numbers.length - 1;

		while (start < end) {
			int sum = numbers[start] + numbers[end];
			if (sum == target) {
				ans[0] = start;
				ans[1] = end;
				break;
			}
			if (sum > target) {
				end--;
			} else {
				start++;
			}

		}

		return ans;

	}
}
