package dp;

import java.util.ArrayList;
import java.util.List;

//https://leetcode.com/problems/palindrome-partitioning/
public class PalindromePartitioning {

	public static void main(String[] args) {
		Solution131 solution131 = new Solution131();
		String s = "aabb";
		List<List<String>> ans = solution131.partition(s);
		System.out.println(ans);
	}

}

class Solution131 {
	private List<List<String>> ans = new ArrayList();

	public List<List<String>> partition(String s) {
		partitionHelper(s, new ArrayList(), 0);
		return ans;

	}

	private void partitionHelper(String s, List<String> comb, int start) {
		if (start == s.length())
			ans.add(new ArrayList(comb));

		for (int i = start; i < s.length(); i++) {
			if (isPalindrome(s, start, i)) {
				comb.add(s.substring(start, i + 1));
				partitionHelper(s, comb, i + 1);
				comb.remove(comb.size() - 1);
			}
		}

	}

	private boolean isPalindrome(String s, int start, int end) {
		while (start < end) {
			if (s.charAt(start++) != s.charAt(end--))
				return false;
		}
		return true;
	}
}
