package dp;

import java.util.ArrayList;
import java.util.List;

public class PalindromePartitioningII {

	public static void main(String[] args) {
		Solution132 solution132 = new Solution132();
		String s = "aab";
		int minCut = solution132.minCut(s);
		System.out.println(minCut);
	}

}

class Solution132 {
	private List<List<String>> ans = new ArrayList();
	private int min = Integer.MAX_VALUE;
	private int[][] memo;

	public int minCut(String s) {
		memo = new int[s.length()][s.length()];
		partitionHelper(s, new ArrayList(), 0, 0);
		return min;
	}

	private void partitionHelper(String s, List<String> comb, int start, int minCut) {
		if (start == s.length()) {
			ans.add(new ArrayList(comb));
			min = Math.min(minCut - 1, min);
		}

		for (int i = start; i < s.length(); i++) {

			if (memo[start][i] > minCut)
				continue;

			if (isPalindrome(s, start, i)) {
				memo[start][i] = minCut + 1;
				comb.add(s.substring(start, i + 1));
				partitionHelper(s, comb, i + 1, minCut + 1);
				comb.remove(comb.size() - 1);
			}
		}

	}

	private boolean isPalindrome(String s, int start, int end) {
		while (start < end) {
			if (s.charAt(start++) != s.charAt(end--))
				return false;
		}
		return true;
	}
}
