/**
 * 
 */
package recur;

import java.util.Objects;

/**
 * ref R-5.8
 */
public class StringToInteger {
	public static void main(String[] args) {

		String input = "13531345";
		String output = convertToInt(input);
		System.out.println(output);

	}

	private static String convertToInt(String input) {
		Objects.requireNonNull(input);
		int l = input.length();
		if (l < 4)
			return input;
		String last = input.substring(l - 3);
		String remain = input.substring(0, l - 3);
		return convertToInt(remain) + "," + last;
	}

}
