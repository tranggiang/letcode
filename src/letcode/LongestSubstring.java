package letcode;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 *
 */
public class LongestSubstring {

	public static void main(String[] args) {

		String s = "abcbabc";
		int i = 0;
		int j = 0;
		int ans = 0;
		int n = s.length();
		Set<Character> set = new HashSet<>();

		while (j < n) {
			if (!set.contains(s.charAt(j))) {
				set.add(s.charAt(j++));
				ans = Math.max(ans, set.size());
			} else {
				set.remove(s.charAt(i++));
			}

		}
		System.out.println(ans);

	}

}
