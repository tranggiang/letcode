package dp;

import java.util.HashMap;
import java.util.Map;

import util.MyUtil;

//https://leetcode.com/problems/jump-game-ii/
public class JumpGameII {

	public static void main(String[] args) {

		Solution10 solution10 = new Solution10();
		int[] nums = MyUtil.randomArray(5);
		solution10.jumpInter(nums);
	}

}

class Solution10 {
	private Map<Integer, Integer> memo = new HashMap();

	public int jump(int[] nums) {

		return minJump(nums, nums.length - 1);

	}

	public int jumpInter(int[] nums) {

		int[] dp = new int[nums.length];
		dp[0] = 0;
		for (int i = 1; i < nums.length; i++) {
			int ans = Integer.MAX_VALUE;
			for (int j = 0; j < i; j++) {
				if (j + nums[j] >= i) {
					ans = Math.min(ans, dp[j]);
					dp[i] = ans + 1;
				}
			}
		}
		return dp[nums.length - 1];

	}

	private int minJump(int[] nums, int index) {
		if (memo.containsKey(index))
			return memo.get(index);
		if (index == 0)
			return 0;
		int totalMin = Integer.MAX_VALUE;
		for (int i = 0; i < index; i++) {

			if (i + nums[i] >= index) {
				totalMin = Math.min(totalMin, minJump(nums, i));
			}

		}
		memo.put(index, 1 + totalMin);
		return 1 + totalMin;
	}
}
