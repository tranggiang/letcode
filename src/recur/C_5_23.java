/**
 * 
 */
package recur;

import java.util.HashMap;
import java.util.Map;

/**
 * C-5.23
 *
 */
public class C_5_23 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int[] a = { 2, 0, 4, 5, 6, 7, 7, 3, 4, 5 };

		Map<Integer, Integer> map = new HashMap<>();

		for (int i = 0; i < a.length; i++) {

			for (int j = i + 1; j < a.length; j++) {
				if (map.containsKey(a[i] + a[j])) {
					System.out.println("index: " + i + " index: " + j);
				} else {
					map.put(a[i], i);
				}
			}
		}

	}

}
