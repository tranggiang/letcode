package twopointers;

import java.util.Arrays;

import util.MyUtil;

//https://leetcode.com/problems/move-zeroes/
public class MoveZeroes {

	public static void main(String[] args) {
		int[] randomArray = MyUtil.randomArray(10);
		System.out.println("Before : " + Arrays.toString(randomArray));
		moveZeroes(randomArray);
		System.out.println("After : " + Arrays.toString(randomArray));
	}

	public static void moveZeroes(int[] nums) {
		int lastNonZero = 0;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != 0) {
				nums[lastNonZero++] = nums[i];
			}
		}
		for (int i = lastNonZero; i < nums.length; i++) {
			nums[i] = 0;
		}
	}

}
