package letcode;

public class PartitionEqualSubsetSum {

	public static void main(String[] args) {
		Solution416 solution416 = new Solution416();

		int[] nums = { 1, 1, 2, 2 };
		boolean canPartition = solution416.canPartition(nums);
		System.out.println(canPartition);
	}

}

class Solution416 {
	private Boolean[][] memo;

	public boolean canPartition(int[] nums) {
		int n = nums.length;

		int sum = 0;
		for (int num : nums)
			sum += num;
		if (sum % 2 == 1)
			return false;
		memo = new Boolean[n + 1][1 + sum / 2];

		return canPartitionHeler(nums, 0, sum / 2);

	}

	private boolean canPartitionHeler(int[] nums, int start, int target) {

		if (target == 0)
			return true;
		if (target < 0)
			return false;
		if (memo[start][target] != null)
			return memo[start][target];

		for (int i = start; i < nums.length; i++) {
			target -= nums[i];
			boolean canPartition = canPartitionHeler(nums, i + 1, target);
			if (canPartition)
				return true;
			target += nums[i];
		}
		memo[start][target] = false;
		return false;
	}
}
