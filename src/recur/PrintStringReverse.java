package recur;

import java.util.Arrays;

import util.MyUtil;

//C-5.17 ,C-5.18
public class PrintStringReverse {

	public static void main(String[] args) {
		String s = "abcde";
		String reversed = reverseString(s);
		System.out.println(reversed);

		String input = "gohangasalamiimalasagnahog";
		boolean isPalindrome = checkPalindrome(input);
		System.out.println("String " + input + " isPalindrome " + isPalindrome);

		int[] rearranged = rearrange(MyUtil.randomArray(10));

		System.out.println("rearranged " + Arrays.toString(rearranged));
	}

	private static int[] rearrange(int[] array) {
		return rearrangeHelper(array, 0, array.length - 1);
	}

	private static int[] rearrangeHelper(int[] array, int left, int right) {

		if (left >= right) {
			return array;
		}
		if (array[left] % 2 == 0) {
			rearrangeHelper(array, ++left, right);
		}
		if (array[right] % 2 == 1)
			return rearrangeHelper(array, left, --right);
		else {
			swap(array, left, right);
			rearrangeHelper(array, ++left, --right);
		}
		return array;
	}

	private static void swap(int[] array, int i, int j) {
		int tem = array[i];
		array[i] = array[j];
		array[j] = tem;
	}

	private static boolean checkPalindrome(String input) {
		if (input == null) {
			return false;
		}
		return checkPalindromeHelper(input, 0, input.length() - 1);
	}

	private static boolean checkPalindromeHelper(String input, int left, int right) {
		if (left >= right) {
			return true;
		}
		if (input.charAt(left++) != input.charAt(right--)) {
			return false;
		}

		return checkPalindromeHelper(input, left, right);
	}

	private static String reverseString(String s) {
		if (s == null) {
			throw new IllegalArgumentException();
		}

		if (s.length() == 1) {
			return s;
		}
		return s.charAt(s.length() - 1) + reverseString(s.substring(0, s.length() - 1));

	}

}
