package dp;

import java.util.HashMap;
import java.util.Map;

import util.MyUtil;

//https://leetcode.com/problems/house-robber/
public class HouseRobber {

	public static void main(String[] args) {
		Solution4 solution4 = new Solution4();
		int max = solution4.rob(MyUtil.randomArray(5));
		System.out.println(max);
	}

}

class Solution4 {
	private Map<Integer, Integer> memo = new HashMap();

	public int rob(int[] nums) {
		return robHelper(nums, nums.length - 1);

	}

	private int robHelper(int[] nums, int index) {
		if (index < 0)
			return 0;
		if (index == 0)
			return nums[0];
		if (memo.containsKey(index))
			return memo.get(index);

		int max = Math.max(robHelper(nums, index - 2) + nums[index], robHelper(nums, index - 1));
		memo.put(index, max);
		return max;
	}

}
