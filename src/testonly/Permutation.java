package testonly;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Permutation {
	static List<List<Integer>> ans = new ArrayList<>();

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		permutaion(list, 0);
		System.out.println(ans.toString());
	}

	private static void permutaion(List<Integer> list, int start) {
		if (start == list.size())
			ans.add(new ArrayList<>(list));

		for (int i = start; i < list.size(); i++) {
			Collections.swap(list, start, i);
			permutaion(list, i + 1);
			Collections.swap(list, start, i);
		}

	}

}
