package dp;

public class NumberMatchingSubsequences {

	public static void main(String[] args) {
		Solution792 solution792 = new Solution792();
		String s = "dsahjpjauf";
		String[] words = { "ahjpjau", "ja", "ahbwzgqnuk", "tnmlanowax" };

		int numMatchingSubseq = solution792.numMatchingSubseq(s, words);
		System.out.println(numMatchingSubseq);
	}

}

class Solution792 {

	public int numMatchingSubseq(String s, String[] words) {

		int numMatching = 0;
		for (String word : words) {
			int num = helper(s.toCharArray(), 0, word.toCharArray(), 0);
			if (num == word.length())
				numMatching++;
		}
		return numMatching;
	}

	private int helper(char[] s, int i, char[] word, int j) {

		if (i == s.length || j == word.length)
			return 0;

		if (s[i] == word[j])
			return 1 + helper(s, i + 1, word, j + 1);
		return helper(s, i + 1, word, j);

	}
}
