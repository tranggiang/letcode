package recur.double_recur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//https://leetcode.com/problems/different-ways-to-add-parentheses/
public class AddParentheses {

	public static void main(String[] args) {
		Solution3 solution = new Solution3();
		String expression = "2*3-4*5";
		List<Integer> ans = solution.diffWaysToCompute(expression);
		System.out.println(ans);
	}

}

class Solution3 {
	private String plus = "+";
	private String minus = "-";
	private String mul = "*";
	private Map<String, List<Integer>> memo = new HashMap<>();

	public List<Integer> diffWaysToCompute(String expression) {
		if (memo.containsKey(expression))
			return memo.get(expression);
		List<Integer> ret = new ArrayList();

		for (int i = 0; i < expression.length() - 1; i++) {
			if (!Character.isDigit(expression.charAt(i))) {
				String ope = expression.substring(i, i + 1);
				List<Integer> leftList = diffWaysToCompute(expression.substring(0, i));
				List<Integer> rightList = diffWaysToCompute(expression.substring(i + 1));
				for (Integer left : leftList)
					for (Integer right : rightList)
						if (plus.equals(ope)) {
							int result = left + right;
							ret.add(result);
						} else if (minus.equals(ope)) {
							int result = left - right;
							ret.add(result);
						} else if (mul.equals(ope)) {
							int result = left * right;
							ret.add(result);
						}

			}

		}
		if (ret.isEmpty())
			ret.add(Integer.valueOf(expression));
		memo.put(expression, ret);
		return ret;

	}

}
