package backtrack;

//https://leetcode.com/problems/word-search/
public class WordSearch {

	public static void main(String[] args) {
		Solution3 solution3 = new Solution3();
		char[][] board = new char[][] { { 'A', 'B', 'C', 'E' }, { 'S', 'F', 'E', 'S' }, { 'A', 'D', 'E', 'E' } };
		String word = "ABCESEEEFS";
		boolean exist = solution3.exist(board, word);
		System.out.println(exist);
	}

}

class Solution3 {
	private char[][] board;
	private String word;

	public boolean exist(char[][] board, String word) {
		this.board = board;
		this.word = word;

		for (int row = 0; row < this.board.length; row++)
			for (int col = 0; col < this.board[0].length; col++) {
				if (board[row][col] == word.charAt(0)) {
					board[row][col] = '#';
					boolean existed = existHelper(row, col, 1);
					board[row][col] = word.charAt(0);
					if (existed)
						return existed;

				}
			}
		return false;

	}

	private boolean existHelper(int row, int col, int index) {
		if (index == word.length()) {
			return true;
		}

		if (goExprole(row, col - 1, index) || goExprole(row, col + 1, index) || goExprole(row + 1, col, index)
				|| goExprole(row - 1, col, index)) {
			return true;
		}
		return false;

	}

	private boolean goExprole(int row, int col, int index) {
		if (col < 0 || col == this.board[0].length || row < 0 || row == this.board.length) {
			return false;
		}

		if (board[row][col] != word.charAt(index)) {
			return false;
		}
		board[row][col] = '#';
		boolean existHelper = existHelper(row, col, index + 1);
		board[row][col] = word.charAt(index);

		return existHelper;
	}
}
