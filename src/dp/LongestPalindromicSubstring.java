package dp;

public class LongestPalindromicSubstring {

	public static void main(String[] args) {
		Solution9 solution9 = new Solution9();
		String s = "cb";
		String longestPalindrome = solution9.longestPalindrome(s);
		System.out.println(longestPalindrome);
	}

}

class Solution9 {
	public String longestPalindrome(String s) {
		int n = s.length();
		boolean[][] dp = new boolean[n][n];

		int max = 0;
		int start = 0;
		int end = 0;
		for (int i = n - 1; i >= 0; i--) {
			for (int j = i; j <= n - 1; j++) {
				if (i == j) {
					dp[i][j] = true;
				} else if (s.charAt(i) == s.charAt(j)) {
					if (j - i == 1)
						dp[i][j] = true;
					else {
						dp[i][j] = dp[i + 1][j - 1];
					}
				}
				if (dp[i][j] && j - i >= max) {
					max = j - i;
					start = i;
					end = j + 1;

				}
			}
		}
		return s.substring(start, end);
	}

}
