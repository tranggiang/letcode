package recur;

import java.util.Arrays;

import util.MyUtil;

public class FindMax {

	public static void main(String[] args) {
		int[] array = MyUtil.randomArray(10);
		int max = findMax(array, 1, array[0]);
		System.out.println("array : " + Arrays.toString(array));
		System.out.println("max :" + max);
	}

	private static int findMax(int[] array, int index, int max) {
		if (index == array.length) {
			return max;
		}
		if (array[index] > max) {
			max = array[index];
		}

		return findMax(array, ++index, max);
	}

}
