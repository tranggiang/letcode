package letcode;

import java.util.HashMap;
import java.util.Map;

public class MaxAreaofIsland {

	public static void main(String[] args) {

		int[][] grid = { { 1, 0 }, { 0, 1 }, { 1, 1 }, };

		Solution1 solution = new Solution1();
		int maxAreaOfIsland = solution.maxAreaOfIsland(grid);
		System.out.println(maxAreaOfIsland);

	}

}

class Solution1 {
	int[][] grid;
	int row;
	int col;
	int max = Integer.MIN_VALUE;
	Map<String, Boolean> visited = new HashMap();

	public int maxAreaOfIsland(int[][] grid) {
		this.grid = grid;
		this.row = grid.length;
		this.col = grid[0].length;

		for (int i = 0; i < this.row; i++)
			for (int j = 0; j < this.col; j++) {
				int localMax = findMax(i, j);
				max = Math.max(localMax, max);
			}
		return max;

	}

	public int findMax(int row, int col) {
		String key = row + " " + col;
		if (visited.containsKey(key)) {
			return 0;
		}

		if (this.grid[row][col] == 1) {
			visited.put(key, true);
			return 1 + goRight(row, col + 1) + goLeft(row, col - 1) + goDown(row + 1, col) + goUp(row - 1, col);
		}

		else
			return 0;
	}

	public int goRight(int row, int col) {
		if (col < this.col) {
			return findMax(row, col);
		} else
			return 0;
	}

	public int goLeft(int row, int col) {
		if (col >= 0) {
			return findMax(row, col);
		} else
			return 0;
	}

	public int goDown(int row, int col) {
		if (row < this.row) {
			return findMax(row, col);
		} else
			return 0;
	}

	public int goUp(int row, int col) {
		if (row >= 0) {
			return findMax(row, col);
		} else
			return 0;
	}
}
