package letcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//https://leetcode.com/problems/combinations/submissions/
public class Combinations {

	private static List<List<Integer>> ans = new ArrayList();

	public static void main(String[] args) {
		int n = 4;
		int k = 3;
		List<List<Integer>> ret = combine(n, k, new LinkedList(), 1);
		System.out.println(ret.toString());
	}

	public static List<List<Integer>> combine(int n, int k, LinkedList<Integer> comb, int start) {
		if (comb.size() == k) {
			ans.add(new LinkedList<>(comb));

		}

		for (int i = start; i < n + 1; i++) {
			comb.add(i);
			combine(n, k, comb, i + 1);
			comb.removeLast();
		}

		return ans;

	}
}
