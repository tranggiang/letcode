package letcode.binary;

public class Search2DMatrix {

	public static void main(String[] args) {

		Solution3 solution3 = new Solution3();
		int[][] matrix = new int[][] { { 1 }, { 3 } };
		int target = 0;
		solution3.searchMatrix(matrix, target);

	}

}

class Solution3 {
	int row;
	int col;
	int[][] matrix;

	public boolean searchMatrix(int[][] matrix, int target) {
		this.row = matrix.length;
		this.col = matrix[0].length;
		this.matrix = matrix;
		int targetRow = findTargetRow(0, row - 1, target);
		return searchTarget(targetRow, target, 0, col - 1);

	}

	public int findTargetRow(int start, int end, int target) {
		if (start == end) {
			return start;
		}
		if (end < 0 || start > end) {
			return -1;
		}
		int mid = (start + end) / 2;
		if (matrix[mid][0] == target) {
			return mid;
		}
		if (matrix[mid][0] > target) {
			end = mid - 1;
		} else {
			start = mid + 1;
		}
		return findTargetRow(start, end, target);

	}

	public boolean searchTarget(int row, int target, int start, int end) {
		if (start > end) {
			return false;
		}
		int mid = (start + end) / 2;
		if (matrix[row][mid] == target) {
			return true;
		}
		if (matrix[row][mid] > target) {
			end = mid - 1;
		} else {
			start = mid + 1;
		}
		return searchTarget(row, target, start, end);

	}
}
