package array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSum {

	public static void main(String[] args) {
		Solution solution = new Solution();
		int[] nums = { -1, 0, 1, 2, -1, -4 };
		List<List<Integer>> ans = solution.threeSum(nums);
		System.out.println(ans);
	}

}

class Solution {
	private List<List<Integer>> ret = new ArrayList();

	public List<List<Integer>> threeSum(int[] nums) {
		Arrays.sort(nums);

		for (int i = 0; i < nums.length; i++) {
			int target = 0 - nums[i];
			if (i == 0 || nums[i] != nums[i - 1])
				threeSumHelper(nums, i + 1, nums.length - 1, target, nums[i]);
		}

		return ret;
	}

	private void threeSumHelper(int[] nums, int start, int end, int target, int num) {

		while (start < end) {
			if (nums[start] + nums[end] == target) {
				ret.add(Arrays.asList(nums[start++], nums[end--], num));
				while (start + 1 < end && nums[start] == nums[start + 1])
					start++;

				while (end - 1 > start && nums[end] == nums[end - 1])
					end--;

			} else if (nums[start] + nums[end] < target) {
				start++;
			} else {
				end--;
			}
		}
	}
}
