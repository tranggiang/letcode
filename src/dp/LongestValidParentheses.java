package dp;

public class LongestValidParentheses {

	public static void main(String[] args) {
		Solution32 solution32 = new Solution32();
		String s = "()(()";
		int longest = solution32.longestValidParentheses(s);
		System.out.println(longest);
	}

}

class Solution32 {
	private int longest = 0;

	public int longestValidParentheses(String s) {
		if (s.isEmpty())
			return 0;
		for (int i = 0; i < s.length(); i++) {
			for (int j = i + 1; j < s.length(); j++) {
				if ((j - i) % 2 == 0)
					continue;
				if (isValid(i, j, s)) {
					longest = Math.max(longest, j - i + 1);
				}
			}
		}
		return longest;

	}

	private boolean isValid(int i, int j, String s) {
		// TODO Auto-generated method stub
		System.out.println("This is a printing message");
		return false;
	}

}
