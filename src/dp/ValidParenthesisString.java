package dp;

//https://leetcode.com/problems/valid-parenthesis-string/
public class ValidParenthesisString {

	public static void main(String[] args) {
		Solution678 solution678 = new Solution678();
		String s = "**((*";
		boolean valid = solution678.checkValidString(s);
		System.out.println(valid);
	}

}

class Solution678 {
	public boolean checkValidString(String s) {
		return badHelper(s, 0, 0, 0);
	}

	private boolean badHelper(String s, int index, int open, int close) {
		System.out.println("Index : " + index);
		if (index == s.length()) {
			if (open == close)
				return true;
			else
				return false;
		}

		if (isOpen(s.charAt(index))) {
			return badHelper(s, index + 1, open + 1, close);
		} else if (isClose(s.charAt(index))) {
			if (close + 1 > open)
				return false;
			return badHelper(s, index + 1, open, close + 1);
		} else {
			// case char is *
			boolean ret = false;
			ret |= badHelper(s, index + 1, open + 1, close);
			if (close + 1 <= open)
				ret |= badHelper(s, index + 1, open, close + 1);
			ret |= badHelper(s, index + 1, open, close);
			return ret;
		}
	}

	private boolean isClose(char charAt) {
		return charAt == ')';
	}

	private boolean isOpen(char charAt) {
		return charAt == '(';
	}
}