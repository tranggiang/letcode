package slidewd;

import java.util.HashMap;
import java.util.Map;

public class PermutationString {

	public static void main(String[] args) {
		Solution solution = new Solution();
		String s1 = "adc";
		String s2 = "dcda";
		boolean checkInclusion = solution.checkInclusion(s1, s2);
		System.out.println(checkInclusion);
	}

}

class Solution {
	public boolean checkInclusion(String s1, String s2) {

		if (s2.length() < s1.length())
			return false;
		Map<Character, Integer> s1Map = new HashMap();
		for (int i = 0; i < s1.length(); i++)
			s1Map.put(s1.charAt(i), s1Map.getOrDefault(s1.charAt(i), 0) + 1);
		Map<Character, Integer> s2Map = new HashMap();
		int index = 0;
		int remove = 0;
		while (index < s2.length()) {
			char key = s2.charAt(index);
			s2Map.put(key, s2Map.getOrDefault(key, 0) + 1);
			index++;
			if (index < s1.length())
				continue;
			if (s1Map.equals(s2Map))
				return true;
			char charAt = s2.charAt(remove);
			Integer value = s2Map.get(charAt);
			if (value == 1)
				s2Map.remove(charAt);
			else
				s2Map.put(charAt, value - 1);
			remove++;
		}

		return false;

	}
}
