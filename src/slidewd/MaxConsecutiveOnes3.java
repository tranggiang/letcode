package slidewd;

public class MaxConsecutiveOnes3 {

	public static void main(String[] args) {
		Solution1004 solution1004 = new Solution1004();
		int[] nums = { 1, 1, 0, 0, 1 };
		int k = 1;
		int max = solution1004.longestOnes(nums, k);
		System.out.println(max);
	}

}

class Solution1004 {

	public int longestOnes(int[] nums, int k) {
		int left = 0;
		int i = 0;
		for (i = 0; i < nums.length; i++) {
			if (nums[i] == 0) {
				k--;
			}
			if (k < 0) {

				k += 1 - nums[left];
				left++;
			}
		}

		return i - left;

	}
}
