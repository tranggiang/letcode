package dp;

import java.util.HashMap;
import java.util.Map;

public class CombinationSumIV {

	public static void main(String[] args) {
		Solution28 solution28 = new Solution28();
		int[] nums = { 1, 2, 3 };
		int target = 4;
		solution28.combinationSum4Inter(nums, target);
	}

}

class Solution28 {
	private Map<Integer, Integer> memo = new HashMap();

	public int combinationSum4(int[] nums, int target) {
		if (target >= 0)
			System.out.println("Target  : " + target);
		if (memo.containsKey(target))
			return memo.get(target);
		if (target == 0)
			return 1;
		if (target < 0)
			return 0;
		int count = 0;

		for (int num : nums) {
			count += combinationSum4(nums, target - num);
		}
		memo.put(target, count);
		return count;

	}

	public int combinationSum4Inter(int[] nums, int target) {
		// minor optimization
		int[] dp = new int[target + 1];
		dp[0] = 1;

		for (int combSum = 1; combSum < target + 1; ++combSum) {
			System.out.println("Target: " + combSum);
			for (int num : nums) {
				if (combSum - num >= 0) {
					System.out.println("Target: " + (combSum - num));
					dp[combSum] += dp[combSum - num];
					// minor optimizaton, early stopping
					// else
					// break;
				}

			}
		}
		return dp[target];
	}

}
