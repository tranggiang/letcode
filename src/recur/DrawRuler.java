package recur;

public class DrawRuler {

	public static void main(String[] args) {
		int lenght = 1;
		int stick = 5;
		drawRuler(lenght, stick);

	}

	private static void drawRuler(int lenght, int stick) { 
		for (int i = 0; i < lenght; i++) {
			drawLineWithNumber(i, stick);
			drawInterval(stick-1);
		}
		drawLineWithNumber(lenght, stick);
	}

	private static void drawInterval(int stick) {
		if (stick > 0) {
			drawInterval(stick - 1);
			drawLine(stick);
			drawInterval(stick - 1);
		}
	}

	private static void drawLine(int stick) {
		for (int i = 0; i < stick; i++) {
			System.out.print("-");
		}
		System.out.println("\n");
	}

	private static void drawLineWithNumber(int number, int stick) {
		for (int i = 0; i < stick; i++) {
			System.out.print("-");
		}
		System.out.print(number);
		System.out.println("\n");
	}

}
