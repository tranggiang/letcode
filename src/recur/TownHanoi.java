package recur;

import java.util.ArrayList;
import java.util.List;

public class TownHanoi {

	public static void main(String[] args) {

		List<Integer> a = new ArrayList<>();
		List<Integer> b = new ArrayList<>();
		List<Integer> c = new ArrayList<>();
		for (int i = 3; i > 0; i--) {
			a.add(i);
		}

		int n = 3;
		townHanoi(n, a, b, c);
		System.out.println(b.toString());

	}

//The pattern here is :
	// Shift 'n-1' disks from 'A' to 'B'.
//	Shift last disk from 'A' to 'C'. 
//	Shift 'n-1' disks from 'B' to 'C'.
	private static void townHanoi(int n, List<Integer> from, List<Integer> to, List<Integer> tem) {
		if (n > 0) {
			townHanoi(n - 1, from, tem, to);
			Integer last = from.remove(from.size() - 1);
			to.add(last);
			townHanoi(n - 1, tem, to, from);
		}

	}

}
