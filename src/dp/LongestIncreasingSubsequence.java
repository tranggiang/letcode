package dp;

import java.util.Arrays;

public class LongestIncreasingSubsequence {

	public static void main(String[] args) {
		Solution15 solution15 = new Solution15();
		int[] randomArray = { 10, 9, 2, 5, 3, 7, 101, 18 };
		int lengthOfLIS = solution15.lengthOfLIS(randomArray);
		System.out.println(lengthOfLIS);
	}

}

class Solution15 {
	public int lengthOfLIS(int[] nums) {
		int[] dp = new int[nums.length];
		Arrays.fill(dp, 1);
		int max = 1;
		for (int i = 1; i < nums.length; i++) {
			for (int j = 0; j < i; j++) {
				if (nums[j] < nums[i]) {
					dp[i] = Math.max(dp[i], dp[j] + 1);
				}
			}
			// dp[i] = 1+ longest;
			max = Math.max(dp[i], max);
		}
		return max;
	}
}
