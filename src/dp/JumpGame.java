package dp;

//https://leetcode.com/problems/jump-game/
public class JumpGame {

	public static void main(String[] args) {
		Solution6 solution6 = new Solution6();
		int[] nums = { 1, 2 };
		boolean canJump = solution6.canJump(nums);
		System.out.println(canJump);

	}

}

class Solution6 {
	public boolean canJump(int[] nums) {
		if (nums.length == 1)
			return true;
		if (nums[0] == 0)
			return false;

		boolean[] jump = new boolean[nums.length];
		jump[0] = true;

		for (int i = 0; i < nums.length - 1; i++) {
			if (jump[i] != true)
				continue;
			if (i + nums[i] >= nums.length - 1)
				return true;
			for (int j = i; j <= i + nums[i]; j++) {

				jump[j] = true;

			}
		}
		return false;
	}
}
