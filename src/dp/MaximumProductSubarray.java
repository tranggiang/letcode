package dp;

import util.MyUtil;

public class MaximumProductSubarray {

	public static void main(String[] args) {
		Solution25 solution25 = new Solution25();
		int[] nums = MyUtil.randomArray(5);
		solution25.maxProduct(nums);
	}

}

class Solution25 {
	public int maxProduct(int[] nums) {
		int maxSofar = nums[0];
		int minSofar = nums[0];
		int max = nums[0];

		for (int i = 1; i < nums.length; i++) {
			int temp = maxSofar;// before change
			maxSofar = Math.max(nums[i], Math.max(nums[i] * minSofar, nums[i] * maxSofar));
			minSofar = Math.min(nums[i], Math.min(nums[i] * minSofar, nums[i] * temp));
			max = Math.max(maxSofar, max);

		}
		return max;

	}
}
