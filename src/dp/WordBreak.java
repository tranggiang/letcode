package dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordBreak {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Solution14 solution14 = new Solution14();
		String s = "leet";
		List<String> wordDict = new ArrayList();
		wordDict.add("leet");
		wordDict.add("ca");
		wordDict.add("code");
		boolean b = solution14.wordBreak(s, wordDict);
		System.out.println(b);
	}

}

class Solution14 {
	private Map<String, Boolean> memo = new HashMap();

	public boolean wordBreak(String s, List<String> wordDict) {
		if (memo.containsKey(s))
			return memo.get(s);
		if (s.isEmpty())
			return true;

		for (String word : wordDict) {
			if (s.startsWith(word) && wordBreak(s.substring(word.length()), wordDict)) {
				memo.put(s, true);

				return true;

			}
		}

		memo.put(s, false);
		return false;
	}

}
