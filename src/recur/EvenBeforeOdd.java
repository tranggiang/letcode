/**
 * 
 */
package recur;

import java.util.Arrays;

/**
 * C-5.20
 *
 */
public class EvenBeforeOdd {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] a = { 2, 3, 4, 5, 6, 7, 7, 3, 4, 0, 5 };

		sort(a);
		System.out.println("after sort : " + Arrays.toString(a));
	}

	private static void sort(int[] a) {
		sortHelper(0, a.length - 1, a);
	}

	private static void sortHelper(int left, int right, int[] a) {

		if (left > right)
			return;
		if (a[left] % 2 == 1 && a[right] % 2 == 1) {
			sortHelper(left, --right, a);
		} else if (a[left] % 2 == 1 && a[right] % 2 == 0) {
			int temp = a[right];
			a[right] = a[left];
			a[left] = temp;
			sortHelper(++left, --right, a);
		} else {
			sortHelper(++left, right, a);
		}

	}

}
