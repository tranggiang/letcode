package letcode.binary;

public class FindFirstandLastPosition {

	public static void main(String[] args) {
		Solution solution = new Solution();
		int[] nums = new int[] { 5, 7, 7, 8, 8, 10 };
		int target = 8;
		int[] searchRange = solution.searchRange(nums, target);
		System.out.println(searchRange);
	}

}

class Solution {
	int target;
	int[] nums;

	public int[] searchRange(int[] nums, int target) {
		if (nums.length < 1) {
			return new int[] { -1, -1 };
		}
		this.target = target;
		this.nums = nums;
		int start = 0;
		int end = nums.length - 1;
		int[] ans = new int[2];
		ans[0] = findStart(start, end);
		ans[1] = findEnd(start, end);
		return ans;
	}

	private int findStart(int start, int end) {
		int index = -1;
		while (start <= end) {
			int mid = (start + end) / 2;
			if (nums[mid] >= target) {

				end = mid - 1;
			} else {
				start = mid + 1;
			}
			if (nums[mid] == target) {
				index = mid;
			}
		}
		return index;
	}

	private int findEnd(int start, int end) {
		int index = -1;
		while (start <= end) {
			int mid = (start + end) / 2;
			if (nums[mid] <= target) {
				start = mid + 1;
			} else {
				end = mid - 1;
			}
			if (nums[mid] == target) {
				index = mid;
			}
		}
		return index;
	}

}