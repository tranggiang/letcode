package recur.double_recur;

//https://leetcode.com/problems/interleaving-string/
public class InterleavingString {

	public static void main(String[] args) {
		Solution solution = new Solution();
		String s1 = "abc";
		String s2 = "def";
		String s3 = "abcdef";
		boolean ans = solution.isInterleave(s1, s2, s3);
		System.out.println(ans);
	}

}

class Solution {
	public boolean helper(String s1, int i, String s2, int j, String res, String s3) {
		if (res.equals(s3) && i == s1.length() && j == s2.length())
			return true;
		boolean ans = false;
		if (i < s1.length())
			ans |= helper(s1, i + 1, s2, j, res + s1.charAt(i), s3);

		if (j < s2.length())
			ans |= helper(s1, i, s2, j + 1, res + s2.charAt(j), s3);

		return ans;

	}

	public boolean isInterleave(String s1, String s2, String s3) {
		if (s1.length() + s2.length() != s3.length()) {
			return false;
		}
		return helper(s1, 0, s2, 0, "", s3);
	}
}
