package letcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Triangle {
	static List<List<Integer>> list = new ArrayList<>();
	private static Map<String, Integer> memoTable = new HashMap<>();

	static int couter = 0;

	public static void main(String[] args) {

		list.add(Arrays.asList(2));
		list.add(Arrays.asList(3, 4));
		list.add(Arrays.asList(6, 5, 7));
		list.add(Arrays.asList(4, 1, 8, 3));
		int minimumTotal = minPath(0, 0);
		System.out.println(minimumTotal);
		System.out.println("number of counter : " + couter);
	}

	private static int minPath(int row, int col) {

		couter++;
		String params = row + ":" + col;
		System.out.println(params);
		if (memoTable.containsKey(params)) {
			return memoTable.get(params);
		}
		int path = list.get(row).get(col);
		if (row < list.size() - 1) {
			path += Math.min(minPath(row + 1, col), minPath(row + 1, col + 1));
		}
		memoTable.put(params, path);
		return path;
	}

}
