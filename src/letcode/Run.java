package letcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import util.MyUtil;

public class Run {

	public static void main(String[] args) {

		int[] nums = { 1, 2, 3 };

		String s = "eabcb";
		List<List<Integer>> subsets =  Solution.subsets2(nums);
		System.out.println(subsets.toString());

	}

}

