/**
 * 
 */
package recur;

/**
 * C-5.13
 *
 */
public class ProductPositiveIntegers {

	public static void main(String[] args) {
		int m = 5;
		int n = 6;
		int product = product(m, n);

		System.out.println(product);
	}

	private static int product(int m, int n) {
		if (n == 1)
			return m;
		return m + product(m, n - 1);
	}

}
