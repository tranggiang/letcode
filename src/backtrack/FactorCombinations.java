package backtrack;

import java.util.ArrayList;
import java.util.List;

public class FactorCombinations {

	public static void main(String[] args) {
		Solution4 solution4 = new Solution4();
		List<List<Integer>> factors = solution4.getFactors(8);
		System.out.println(factors);
	}

}

class Solution4 {
	private List<List<Integer>> ans = new ArrayList<>();

	public List<List<Integer>> getFactors(int n) {
		getFactorsHelper(n, new ArrayList(), 2);
		return ans;

	}

	private void getFactorsHelper(int n, List<Integer> comb, int start) {

		if (n == 1) {
			if (comb.size() > 1)
				ans.add(new ArrayList<>(comb));

		}

		for (int i = start; i <= n; i++) {
			if (n % i == 0) {
				comb.add(i);
				getFactorsHelper(n / i, comb, i);
				comb.remove(comb.size() - 1);
			}
		}

	}
}
