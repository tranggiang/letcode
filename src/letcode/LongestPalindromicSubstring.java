/**
 * 
 */
package letcode;

/**
 * @author Admin
 *
 */
public class LongestPalindromicSubstring {

	/**
	 * @param args https://leetcode.com/problems/longest-palindromic-substring/
	 * 
	 *             https://www.geeksforgeeks.org/longest-palindrome-substring-set-1/
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static String longestPalindrome(String s) {
		int n = s.length();
		String longest = "";
		for (int i = 0; i <= n; i++) {
			int left = i - 1;
			int right = i + 1;
			while (left >= 0 && right <= n) {
				if (s.charAt(left) == s.charAt(right)) {
					if (right - left > longest.length()) {
						longest = s.substring(left, right);
					}
					left--;
					right++;
				} else {
					break;
				}
			}
		}

		return s;
	}
}
