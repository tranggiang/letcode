package dp;

public class PredictWinner {

	public static void main(String[] args) {
		Solution3 solution3 = new Solution3();
		int[] nums = { 1, 5, 2 };
		boolean canWin = solution3.predictTheWinner(nums);
		System.out.println(canWin);
	}

}

class Solution3 {
	public boolean predictTheWinner(int[] nums) {
		int l = nums.length;
		int sum1 = 0;
		int sum2 = 0;
		int count = 0;
		int start = 0;
		int end = l - 1;
		while (start <= end) {
			if (nums[start] > nums[end]) {
				sum1 += nums[start];
				start++;
				count++;
			} else {
				sum1 += nums[end];
				end--;
				count++;
			}

			if (count < l) {
				if (nums[start] > nums[end]) {
					sum2 += nums[end];
					end--;
					count++;
				} else {
					sum2 += nums[start];
					start++;
					count++;
				}

			}

		}

		return sum1 >= sum2;
	}
}
