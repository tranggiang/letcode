/**
 * 
 */
package recur;

import java.util.Arrays;

/**
 * C-5.21
 *
 */
public class SortByK {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = { 2, 3, 4, 5, 6, 7, 7, 3, 4, 0, 5 };
		int k = 4;
		sortByK(k, a);
		System.out.println("After sort " + Arrays.toString(a));
	}

	private static void sortByK(int k, int[] a) {
		helper(k, a, 0, a.length - 1);
	}

	private static void helper(int k, int[] a, int left, int right) {
		if (left > right) {
			return;
		}
		if (a[left] > k) {
			if (a[right] > k) {
				helper(k, a, left, --right);
			} else if (a[right] == k) {
				swap(left, right, a);
				helper(k, a, ++left, --right);
			} else {
				swap(left, right, a);
				helper(k, a, ++left, --right);
			}
		} else if (a[left] == k) {
			if (a[right] > k) {
				helper(k, a, --left, --right);
			} else if (a[right] == k) {
				swap(left, right, a);
				helper(k, a, ++left, right);
			} else {
				swap(left, right, a);
				helper(k, a, ++left, right);
			}
		} else {
			if (a[right] > k) {
				helper(k, a, ++left, --right);
			} else if (a[right] == k) {
				helper(k, a, ++left, right);
			} else {
				helper(k, a, ++left, right);
			}
		}

	}

	private static void swap(int left, int right, int[] a) {
		int temp = a[left];
		a[left] = a[right];
		a[right] = temp;

	}

}
