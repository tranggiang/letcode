package letcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class Solution {
	public static int lengthOfLongestSubstring(String s) {
		int ans = 0;
		int i = 0;
		int j = 0;
		int n = s.length();
		Set<Character> set = new HashSet<Character>();

		while (i < n && j < n) {
			if (!set.contains(s.charAt(j))) {
				set.add(s.charAt(j++));
				ans = Math.max(ans, j - i);
			} else {
				set.remove(s.charAt(i++));
			}
		}
		return ans;
	}

	// find 2nd largest number in array
	public static int secoundMax(int[] nums) {

		int max = nums[0];
		int secondMax = Integer.MIN_VALUE;

		for (int i = 1; i < nums.length; i++) {
			if (nums[i] > max) {
				secondMax = max;
				max = nums[i];
			} else if (nums[i] > secondMax && nums[i] < max) {
				secondMax = nums[i];
			}

		}
		return secondMax;

	}

	public static boolean increasingTriplet(int[] nums) {

		int minOne = Integer.MAX_VALUE;
		int minTwo = Integer.MAX_VALUE;

		for (int num : nums) {
			if (num < minOne) {
				minOne = num;
			}
			if (num > minOne) {
				minTwo = Math.min(minTwo, num);
			}
			if (num > minTwo)
				return true;
		}
		return false;

	}

	public static List<List<String>> groupAnagrams(String[] strs) {

		int n = strs.length;
		Map<Integer, List<String>> map = new HashMap();
		for (int i = 0; i < n; i++) {

			int sum = 0;
			for (int k = 0; k < strs[i].length(); k++) {
				sum = sum + (int) strs[i].charAt(k);
			}

			if (!map.containsKey(sum)) {
				map.put(sum, new ArrayList<>());
			}
			map.get(sum).add(strs[i]);

		}
		;
		return new ArrayList<>(map.values());

	}

	public static int fib(int n) {

		if (n == 0)
			return 0;
		if (n == 1)
			return 1;

		int f0 = 0;
		int f1 = 1;
		for (int i = 2; i <= n; i++) {
			int fi = f0 + f1;
			f0 = f1;
			f1 = fi;
		}
		return f1;

	}

	// slow
	public static int threeSumClosest(int[] nums, int target) {
		List<List<Integer>> ans = new ArrayList();

		int n = nums.length;
		int closest = Integer.MAX_VALUE;
		for (int i = 0; i < n - 2; i++)
			for (int j = i + 1; j < n - 1; j++)
				for (int k = j + 1; k < n; k++)
					if (Math.abs(nums[i] + nums[j] + nums[k] - target) <= Math.abs(closest)) {
						closest = nums[i] + nums[j] + nums[k] - target;
						// sum = nums[i] + nums[j] + nums[k];
					}

		return closest + target;

	}

	public static int threeSumClosestFast(int[] nums, int target) {
		int n = nums.length;
		int closest = Integer.MAX_VALUE;
		Arrays.sort(nums);
		for (int i = 0; i < n; i++) {
			int low = i + 1;
			int hi = n - 1;
			while (low < hi) {
				int sum = nums[i] + nums[low] + nums[hi];
				if (Math.abs(sum - target) < Math.abs(closest))
					closest = sum - target;
				if (sum < target)
					++low;
				else
					--hi;

			}
		}
		return closest + target;
	}

	public static List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> ans = new ArrayList();
		int n = nums.length;
		Arrays.sort(nums);

		for (int i = 0; i < n; i++) {
			if (i > 0 && nums[i] == nums[i - 1])
				continue;
			int lo = i + 1;
			int hi = n - 1;
			while (lo < hi) {
				int sum = nums[i] + nums[lo] + nums[hi];
				if (sum == 0) {
					ans.add(Arrays.asList(nums[i], nums[lo], nums[hi]));
					++lo;
					--hi;
					while (lo < hi && nums[lo] == nums[lo - 1])
						++lo;
					while (lo < hi && nums[hi] == nums[hi + 1])
						--hi;

				}

				else if (sum > 0)
					--hi;
				else
					++lo;
			}
		}

		return ans;

	}

	public static String longestPalindromeSLOW(String s) {
		int n = s.length();
		for (int i = n; i >= 0; i--) {
			int start = 0;
			int end = i;
			while (end <= n) {
				String ans = checkString(s.substring(start, end));
				if (ans != null)
					return ans;
				start++;
				end++;
			}

		}
		// should never happen
		return null;

	}

	public static String longestPalindrome(String s) {
		int n = s.length();
		String longest = s.substring(0, 1);
		for (int i = 0; i < n; i++) {
			String expand = expand(s, i, i);
			if (longest.length() < expand.length())
				longest = expand;
			expand = expand(s, i, i + 1);
			if (longest.length() < expand.length())
				longest = expand;

		}

		return longest;
	}

	private static String expand(String s, int left, int right) {

		while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
			left--;
			right++;
		}
		return s.substring(left + 1, right);
	}

	private static String checkString(String sub) {
		System.out.println(sub);

		int n = sub.length();
		int left = 0;
		int right = n - 1;
		while (left <= right) {
			if (sub.charAt(left) != sub.charAt(right))
				return null;
			left++;
			right--;
		}
		return sub;

	}

	public static List<List<Integer>> subsets2(int[] nums) {
		List<List<Integer>> result = new ArrayList<>();
		result.add(new ArrayList<>());
		if (nums == null || nums.length == 0) {
			return result;
		}
		int s = 0;
		for (int n : nums) {
			s = result.size();
			for (int i = 0; i < s; i++) {
				List<Integer> set = new ArrayList<>(result.get(i));
				set.add(n);
				result.add(set);
			}
		}
		return result;
	}

}