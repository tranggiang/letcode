package backtrack;

import java.util.ArrayList;
import java.util.List;

public class LetterCasePermutation {

	public static void main(String[] args) {
		Solution7 solution7 = new Solution7();
		List<String> letterCasePermutation = solution7.letterCasePermutation("abc");
		System.out.println(letterCasePermutation);
	}

}

class Solution7 {
	private List<String> ans;

	public List<String> letterCasePermutation(String s) {
		ans = new ArrayList<>();
		letterCasePermutationHelper(s, 0);
		return ans;

	}

	private void letterCasePermutationHelper(String s, int start) {
		ans.add(s);
		for (int i = start; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (Character.isLowerCase(ch)) {
				letterCasePermutationHelper(s.substring(0, i) + Character.toUpperCase(ch) + s.substring(i + 1), i + 1);
			} else {
				letterCasePermutationHelper(s.substring(0, i) + Character.toLowerCase(ch) + s.substring(i + 1), i + 1);
			}

		}

	}
}
