package dp;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.com/problems/integer-break/
public class IntegerBreak {

	public static void main(String[] args) {
		Solution18 solution18 = new Solution18();
		int max = solution18.integerBreakInter(10);
		System.out.println(max);
	}

}

class Solution18 {
	private Map<Integer, Integer> memo = new HashMap();

	public int integerBreak(int n) {
		if (memo.containsKey(n))
			return memo.get(n);
		if (n == 2)
			return 1;
		int ans = 0;
		for (int i = 1; i <= n / 2; i++) {
			int product = i * Math.max((n - i), integerBreak(n - i));
			ans = Math.max(ans, product);

		}
		memo.put(n, ans);
		return ans;

	}

	public int integerBreakInter(int n) {
		if (n == 0)
			return 0;
		if (n == 1)
			return 1;

		int[] dp = new int[n + 1];

		dp[1] = 1;

		for (int i = 2; i <= n; i++) {
			for (int j = 1; j <= i / 2; j++) {
				dp[i] = Math.max(dp[i], j * Math.max(i - j, dp[i - j]));
			}
		}

		return dp[n];
	}
}
