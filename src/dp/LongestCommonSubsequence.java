package dp;

//https://leetcode.com/problems/longest-common-subsequence/
// wow be able to solve this myself but it run very slow.!!!
public class LongestCommonSubsequence {

	public static void main(String[] args) {
		Solution solution = new Solution();

		String text1 = "abcde";
		String text2 = "ace";
		int longestCommonSubsequence = solution.longestCommonSubsequence(text1, text2);
		System.out.println(longestCommonSubsequence);
	}

}

class Solution {
	String shortString;
	String longString;
	int max = 0;

	public int longestCommonSubsequence(String text1, String text2) {
		int[][] dp = new int[text1.length() + 1][text2.length() + 1];

		for (int i = text1.length() - 1; i >= 0; i--)
			for (int j = text2.length() - 1; j >= 0; j--) {
				if (text1.charAt(i) == text2.charAt(j)) {
					dp[i][j] = 1 + dp[i + 1][j + 1];
				} else {
					dp[i][j] = Math.max(dp[i + 1][j], dp[i][j + 1]);
				}
			}

		return dp[0][0];
	}

	public int longestCommonSubsequence2(String text1, String text2) {
		int[][] dp = new int[text1.length() + 1][text2.length() + 1];

		for (int i = 0; i < text1.length(); i++) {
			for (int j = 0; j < text2.length(); j++) {
				if (text1.charAt(i) != text2.charAt(j)) {
					dp[i + 1][j + 1] = Math.max(dp[i + 1][j], dp[i][j + 1]);
				} else {
					dp[i + 1][j + 1] = 1 + dp[i][j];
				}
			}
		}

		return dp[text1.length()][text2.length()];

	}

}
