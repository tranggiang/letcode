package letcode;

//https://leetcode.com/explore/interview/card/top-interview-questions-easy/97/dynamic-programming/566/
public class MaxSubArray {

	public static void main(String[] args) {
		int[] nums = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
		int maxSubArray = maxSubArray(nums);
		System.out.println(maxSubArray);
	}

	public static int maxSubArray(int[] nums) {
		int maxSum = nums[0];
		int currentMax = nums[0];
		for (int i = 1; i < nums.length; i++) {
			currentMax = Math.max(currentMax + nums[i], nums[i]);
			maxSum = Math.max(currentMax, maxSum);
			;

		}
		return maxSum;

	}

}
