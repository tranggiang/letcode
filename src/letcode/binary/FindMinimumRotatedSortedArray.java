package letcode.binary;

public class FindMinimumRotatedSortedArray {

	public static void main(String[] args) {
		Solution2 solution2 = new Solution2();
		int[] nums = { 4, 5, 6, 7, 0, 1, 2 };
		solution2.findMin(nums);
	}

}

class Solution2 {
	public int findMin(int[] nums) {
		int start = 0;
		int end = nums.length - 1;
		return findMinHelper(start, end, nums);

	}

	public int findMinHelper(int start, int end, int[] nums) {
		if (start == end) {
			return nums[start];
		}

		if (end - start == 1)
			return Math.min(nums[start], nums[end]);

		int mid = (start + end) / 2;
		// left is sorted
		if (nums[mid] > nums[start]) {
			int localMin = nums[start];
			return Math.min(localMin, findMinHelper(mid + 1, end, nums));
		}
		// right is sorted
		else {
			int localMin = nums[mid];
			return Math.min(localMin, findMinHelper(start, mid - 1, nums));
		}

	}
}
