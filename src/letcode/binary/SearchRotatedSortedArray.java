package letcode.binary;

public class SearchRotatedSortedArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Solution4 solution4 = new Solution4();
		int[] nums = new int[] { 4, 5, 6, 7, 0, 1, 2 };
		int target = 0;
		solution4.search(nums, target);
	}

}

class Solution4 {
	public int search(int[] nums, int target) {
		return searchHelper(nums, target, 0, nums.length - 1);
	}

	public int searchHelper(int[] nums, int target, int start, int end) {

		if (start > end || end < 0) {
			return -1;
		}

		int mid = (start + end) / 2;
		if (nums[mid] == target) {
			return mid;
		}
		if (nums[mid] > nums[start]) {
			if (nums[start] <= target && nums[mid] > target) {
				end = mid - 1;
			} else {
				start = mid + 1;
			}
		} else {
			if (nums[mid] < target && nums[end] >= target) {
				start = mid + 1;
			} else {
				end = mid - 1;
			}
		}
		return searchHelper(nums, target, start, end);
	}
}
