/**
 * 
 */
package recur;

/**
 * C-5.15
 */
public class SubSet {

	public static void main(String[] args) {

		String input = "ABC";
		subset(input);
	}

	private static void subset(String input) {
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (i < input.length()) {
				String remain = input.substring(i + 1);
				subSetHelper(String.valueOf(c), remain);
			}
		}
	}

	private static void subSetHelper(String subSet, String remain) {
		System.out.println(subSet);

		for (int i = 0; i < remain.length(); i++) {
			String subset = subSet + remain.charAt(i);
			if (i < remain.length()) {
				subSetHelper(subset, remain.substring(i + 1));
			}
		}
	}
}
