package dp;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.com/problems/edit-distance/
public class EditDistance {

	public static void main(String[] args) {
		Solution19 solution19 = new Solution19();
		// String word1 = "horse";
		// String word2 = "ros";
		String word1 = "abc";
		String word2 = "bcd";
		int min = solution19.minDistanceInter(word1, word2);
		System.out.println(min);

	}

}

class Solution19 {
	private Map<String, Integer> memo = new HashMap();

	public int minDistance(String word1, String word2) {

		return minDistanceHelper(word1, word2, 0, 0);

	}

	public int minDistanceInter(String word1, String word2) {

		int[][] dp = new int[word1.length() + 1][word2.length() + 1];
		for (int i = 0; i <= word2.length(); i++) {
			dp[word1.length()][i] = word2.length() - i;
		}
		for (int i = 0; i <= word1.length(); i++) {
			dp[i][word2.length()] = word1.length() - i;
		}

		for (int i = word1.length() - 1; i >= 0; i--)
			for (int j = word2.length() - 1; j >= 0; j--) {
				if (word1.charAt(i) == word2.charAt(j)) {
					dp[i][j] = dp[i + 1][j + 1];
				} else {
					int ans = Integer.MAX_VALUE;
					dp[i][j] = Math.min(Math.min(ans, dp[i][j + 1]), Math.min(dp[i + 1][j], dp[i + 1][j + 1])) + 1;
				}
			}
		return dp[0][0];

	}

	private int minDistanceHelper(String word1, String word2, int w1, int w2) {
		String key = w1 + ":" + w2;
		if (memo.containsKey(key))
			return memo.get(key);
		if (w1 >= word1.length()) {
			return word2.length() - w2;
		}
		if (w2 >= word2.length())
			return word1.length() - w1;
		int ans = Integer.MAX_VALUE;
		if (word1.charAt(w1) == word2.charAt(w2)) {
			return minDistanceHelper(word1, word2, w1 + 1, w2 + 1);
		} else {

			int optInsert = 1 + minDistanceHelper(word1, word2, w1, w2 + 1);
			ans = Math.min(ans, optInsert);

			int optDelete = 1 + minDistanceHelper(word1, word2, w1 + 1, w2);
			ans = Math.min(ans, optDelete);

			int optReplace = 1 + minDistanceHelper(word1, word2, w1 + 1, w2 + 1);
			ans = Math.min(ans, optReplace);
		}
		memo.put(key, ans);
		return ans;
	}
}
