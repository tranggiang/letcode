package slidewd;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class LongestSubstring159 {

	public static void main(String[] args) {
		Solution159 solution159 = new Solution159();
		String s = "ccaabbb";
		int max = solution159.lengthOfLongestSubstringTwoDistinct(s);
		System.out.println(max);
	}

}

class Solution159 {
	public int lengthOfLongestSubstringTwoDistinct(String s) {
		Map<Character, Integer> map = new HashMap();

		int index = 0;
		int max = 0;
		for (int i = 0; i < s.length(); i++) {
			char charAt = s.charAt(i);
			map.put(charAt, i);
			if (map.size() > 2) {
				Integer smallIndex = Collections.min(map.values());
				map.remove(s.charAt(smallIndex));
				index = smallIndex + 1;
			}
			if (i - index > max)
				max = i - index;
		}
		return max + 1;

	}
}