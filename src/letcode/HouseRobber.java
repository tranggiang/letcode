package letcode;

public class HouseRobber {

	public static void main(String[] args) {
		int[] nums = { 1, 4, 6, 3, 7 };
		rob(nums);
	}

	public static int rob(int[] nums) {
		if (nums.length == 1) {
			return nums[0];
		}
		if (nums.length == 2) {
			return Math.max(nums[0], nums[1]);
		}

		int[] memo = new int[nums.length + 1];
		memo[0] = 0;
		memo[1] = nums[0];

		for (int i = 1; i < nums.length; i++) {
			memo[i + 1] = Math.max(memo[i], memo[i - 1] + nums[i]);
		}
		return memo[nums.length];

	}
}
