package unknown;

import java.util.LinkedList;
import java.util.List;

public class BackspaceStringCompare {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Solution solution = new Solution();
		String s = "y#fo##f";
		String t = "y#f#o##f";
		solution.backspaceCompare(s, t);

	}

}

class Solution {
	public boolean backspaceCompare(String s, String t) {
		List<Character> firsList = convertToLinkedList(s);
		List<Character> secondList = convertToLinkedList(t);
		if (firsList.size() != secondList.size())
			return false;
		for (int i = 0; i < firsList.size(); i++) {
			if (firsList.get(i) != secondList.get(i))
				return false;
		}
		return true;

	}

	private List convertToLinkedList(String s) {
		LinkedList<Character> list = new LinkedList();
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '#') {
				if (!list.isEmpty())
					list.removeLast();
			} else
				list.add(s.charAt(i));
		}
		return list;
	}

}
