package slidewd;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//https://leetcode.com/problems/longest-substring-without-repeating-characters/
public class LongestSubstring {

	public static void main(String[] args) {
		lengthOfLongestSubstring("abcdc");

	}

	public static int lengthOfLongestSubstringBetter(String s) {

		Map<Character, Integer> map = new HashMap<>();
		int start = 0, len = 0;

		// abba
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (map.containsKey(c)) {
				if (map.get(c) >= start)
					start = map.get(c) + 1;
			}
			len = Math.max(len, i - start + 1);
			map.put(c, i);
		}

		return len;
	}

	public static int lengthOfLongestSubstring(String s) {
		int max = 0;
		int index = 0;
		Set<Character> set = new HashSet<>();
		char[] charArray = s.toCharArray();
		for (char c : charArray) {
			while (set.add(c) == false) {
				set.remove(charArray[index++]);
			}
			if (set.size() > max)
				max = set.size();

		}
		return max;

	}

}
