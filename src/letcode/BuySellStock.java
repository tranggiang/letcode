/**
 * 
 */
package letcode;

/**
 * @author Admin
 *         https://leetcode.com/explore/interview/card/top-interview-questions-easy/97/dynamic-programming/572/
 */
public class BuySellStock {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] prices = { 7, 1, 5, 3, 6, 4 };
		int maxProfit = maxProfit(prices);
		System.out.println(maxProfit);
	}

	public static int maxProfit(int[] prices) {
		int minprice = Integer.MAX_VALUE;
		int maxprofit = 0;
		for (int i = 0; i < prices.length; i++) {
			if (prices[i] < minprice)
				minprice = prices[i];
			else if (prices[i] - minprice > maxprofit)
				maxprofit = prices[i] - minprice;
		}
		return maxprofit;

	}

}
